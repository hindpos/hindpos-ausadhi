#!/bin/sh
mv ~/.mongo ~/.mongo.save
mkdir ~/.mongo
rm -rf ~/.invoice
unzip invoice.zip -d ~
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
if [[ `lsb_release -rs` == "14.04" ]]
then
  echo "deb [ arch=amd64,arm64,i386 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
else
  echo "deb [ arch=amd64,arm64,i386 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
fi
sudo apt-get update
sudo apt install mongodb-org
sudo dpkg -i *.deb
sudo apt install -f
