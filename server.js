const ElectronPDF = require('electron-pdf');
const shutdown = require('electron-shutdown-command');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const Printer = require('printer');
const AltPrinter = require('node-printer');
var cors = require('cors');

const { dataToHTML } = require('./packages/hindpos-ausadhi-bill');

const appExpress = express();
appExpress.use(cors());
appExpress.use(bodyParser.json());
appExpress.use('/static', express.static(path.join(__dirname, 'packages', 'hindpos-ausadhi-front', 'dist', 'static')));

const exporter = new ElectronPDF({resilient: true});

const invoiceFile = path.join(process.env.HOME, '.invoice', 'invoice.html');
const invoicePdf = path.join(process.env.HOME, '.invoice', 'invoice.pdf');

let printer = Printer.getDefaultPrinterName();
let altPrinter = new AltPrinter(printer);

// Get Printer List
appExpress.get('/print/list', (req, res) => {
  res.send({
    list: Printer.getPrinters()
      .map(printer => (printer.options && printer.options['printer-make-and-model']) || printer.name)
  });
});

appExpress.post('/print/set', (req, res) => {
  printer = req.body.printer;
  altPrinter = new AltPrinter(printer);
  res.send({success: true});
});

// Serves print server
appExpress.post('/print', async (req, res) => {
  const data = req.body.data;
  if (['A4', 'A5', 'Letter'].indexOf(req.body.pdfOptions.pageSize) > -1) {
    dataToHTML(data);
    const pdfOptions = req.body.pdfOptions || {
      pageSize: "A4"
    };
    const jobOptions = {
      inMemory: false,
    };
    const printOptions = req.body.printOptions || {
      media: pdfOptions.pageSize,
      'fit-to-page': true,
    };
    exporter.createJob(invoiceFile, invoicePdf, pdfOptions, jobOptions).then(job => {
      job.on('job-complete', (r) => {
        Printer.printFile({
          filename: invoicePdf,
          printer: printer || Printer.getDefaultPrinterName(),
          type: 'PDF',
          options: printOptions,
          error: (err) => {
            res.send({
              success: false,
              error: err.message,
            });
            console.log(err.message);
          },
          success: (jid) => {
            res.send({
              success: true,
              jobId: jid,
            });
          }
        });
      });
      job.render();
    });
  } else if (req.body.pdfOptions.pageSize === 'Thermal') {
    await altPrinter.printText(data, {
      media: 'Custom.82.5x207mm',
      cpi: 12,
      lpi: 7,
      'page-left': 15,
      'page-top': 10,
      'page-bottom': 30,
    });
    res.send({success: true});
  }
});

appExpress.get('/shutdown', (req, res) => {
  res.send({success: true});
  shutdown.shutdown({
    force: true,
    quitapp: true
  });
});

/* serves main page */
appExpress.get("/", function(req, res) {
  res.sendFile(path.join(__dirname, 'packages', 'hindpos-ausadhi-front', 'dist', 'index.html'))
});

const port = process.env.HINDPOS_FRONT_PORT || 5000;

exporter.on('charged', () => {
  appExpress.listen(port, function() {
    console.log("Listening on " + port);
  });
});

exporter.start();
